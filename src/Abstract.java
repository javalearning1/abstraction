interface Doginterface{
    void bark();
    void poop();
    // Interface allows only the list and no body and instead of extend use implements!!
}
abstract class Dog{
    public void bark(){
        System.out.println("Lol lol lol");
    }
    public abstract void poop();
}
class Bow extends Dog{
    public void poop(){
        System.out.println("dog pooped");
    }

}
public class Abstract {
    public static void main(String[] args) {
        Bow d = new Bow();
        d.bark();
        d.poop();

    }
}
